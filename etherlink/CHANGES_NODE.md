# Changelog

## Notes

Currently supported kernel version: 32f957d52ace920916d54b9f02a2d32ee30e16b3

## Version Next

### Features

- Stream the L2 blocks to give a faster and more consistent inclusion of
  transactions on the L1. (!11102)

### Bug fixes

### Breaking changes

### Internal
